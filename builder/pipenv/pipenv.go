package pipenv

import (
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
)

const (
	pathPipenv         = "pipenv"
	pathPipdeptreeJSON = "pipdeptree.json"
)

// Builder generates dependency lists for pipenv projects
type Builder struct {
}

// Build generates a dependency list for a Pipfile, and returns its path
func (b Builder) Build(input string) (string, error) {
	// install dependencies using pipenv
	if err := b.installDeps(input); err != nil {
		return "", err
	}

	// save JSON output of "pipenv graph"
	return b.createGraph(input)
}

// installDeps installs pipenv dependencies through pipenv install
// first try is to install without specifying the python path (which forces a virtualenv reinstall)
// works when user's Pipfile doesn't require a different python version and uses PIPENV_DEFAULT_PYTHON_VERSION if set
// if that fails, try again with python path specified
func (b Builder) installDeps(input string) error {
	install := func(args ...string) error {
		args = append([]string{"install"}, args...)
		cmd := exec.Command(pathPipenv, args...)
		cmd.Dir = filepath.Dir(input)
		cmd.Env = os.Environ()
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)
		return err
	}

	err := install()
	if err != nil {
		log.Debug("pipenv install failed, trying again with forced python path")
		err = install("--python", "/usr/local/bin/python")
	}
	return err
}

func (b Builder) createGraph(input string) (string, error) {
	dir := filepath.Dir(input)
	cmd := exec.Command("pipenv", "graph", "--json")
	cmd.Dir = dir
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	// create output file
	output := filepath.Join(dir, pathPipdeptreeJSON)
	f, err := os.Create(output)
	if err != nil {
		return output, err
	}
	defer f.Close()
	cmd.Stdout = f

	// start and wait
	err = cmd.Start()
	if err != nil {
		return output, err
	}
	return output, cmd.Wait()
}

func init() {
	builder.Register("pipenv", &Builder{})
}
