package libfinder

import (
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Flags generates CLI flags to configure the finder
func Flags() []cli.Flag {
	// NOTE: DS_EXCLUDED_PATHS is already returned by finder.Flag
	// and it cannot be redefined.
	return []cli.Flag{}
}

// NewFinder initializes a new finder
func NewFinder(c *cli.Context) (*Finder, error) {
	filter, err := pathfilter.NewFilter(c)
	if err != nil {
		return nil, err
	}

	finder := &Finder{
		IsExcludedPath: filter.IsExcluded,
	}

	return finder, nil
}

// Finder finds vendored libraries in a directory
type Finder struct {
	// IsExcludedPath is a function that returns true when a file path is excluded
	IsExcludedPath func(string) bool
}

// FindLibraries finds vendored libraries in a directory
func (f Finder) FindLibraries(root string) ([]Library, error) {
	libs := []Library{}

	// run retire and decode JSON document
	doc, err := retireJs(root)
	if err != nil {
		return nil, err
	}

	// convert to libraries
	absRoot, err := filepath.Abs(root)
	if err != nil {
		return nil, err
	}
	for _, data := range doc.Data {
		// get relative path
		relPath, err := filepath.Rel(absRoot, data.File)
		if err != nil {
			return nil, err
		}

		// iterate affected components
		for _, result := range data.Results {
			lib := Library{
				Path:        relPath,
				PackageType: parser.PackageTypeNpm,
				Package: parser.Package{
					Name:    result.Component,
					Version: result.Version,
				},
			}
			libs = append(libs, lib)
		}
	}

	return f.filter(libs), nil
}

// filter removes libraries that match the excluded paths
func (f Finder) filter(libs []Library) []Library {
	result := []Library{}
	for _, lib := range libs {
		if f.IsExcludedPath(lib.Path) {
			log.Debugf("skip excluded path: %s", lib.Path)
			continue
		}
		result = append(result, lib)
	}
	return result
}
