package libfinder

import (
	"encoding/json"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

const (
	filenameJsonOutput = "retire-jsrepository.json"
)

// retirJsonDoc represents a JSON document created by retire
// when the output format is set to "json"
type retireJsonDoc struct {
	Data []struct {
		File    string
		Results []struct {
			Version   string
			Component string
		}
	}
	Errors []string
}

// retireJs scans JavaScript vendored libaries
// by running retire --js, decodes the JSON output,
// and returns the decoded document.
//
// The "json" output format is used because it contains
// error messages that are not in the std output or std error.
// The "jsonsimple" output format doesn't include error messages.
//
func retireJs(dir string) (*retireJsonDoc, error) {
	outputPath := filepath.Join(dir, filenameJsonOutput)

	// run retire --js command in the target directory
	// and create a JSON output document in the same directory
	args := []string{
		"--js",
		"--outputformat", "json",
		"--outputpath", filenameJsonOutput,
		"--exitwith", "0",
	}
	// use pre-installed jsrepository.json file if available;
	// if not provided, retire attempts to fetch it from GitHub,
	// and HTTP(S) proxies are not supported because --proxy isn't set
	if jsRepo := os.Getenv("GEMNASIUM_RETIREJS_JS_ADVISORY_DB"); jsRepo != "" {
		args = append(args, "--jsrepo", jsRepo)
	}
	cmd := exec.Command("retire", args...)
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	if err != nil {
		// log command line and possible output
		log.Errorf("%s\n%s", cmd.String(), output)

		// attempt to extract errors from JSON doc, and log them
		if f, err := os.Open(outputPath); err == nil {
			defer f.Close()
			doc := &retireJsonDoc{}
			if err := json.NewDecoder(f).Decode(&doc); err == nil {
				for _, errMsg := range doc.Errors {
					log.Error(errMsg)
				}
			}
		}

		return nil, err
	}
	log.Debugf("%s\n%s", cmd.String(), output)

	// open output file
	f, err := os.Open(outputPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// decode document
	doc := &retireJsonDoc{}
	err = json.NewDecoder(f).Decode(&doc)
	return doc, err
}
