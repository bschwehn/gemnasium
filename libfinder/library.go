package libfinder

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"

type Library struct {
	Path        string
	PackageType parser.PackageType
	Package     parser.Package
}
