package finder

import (
	"errors"
	"os"
	"path/filepath"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
)

// SearchMode defines what should be skipped after there is match
type SearchMode int

const (
	// SearchAll is when the finder search for all compatible projects
	SearchAll SearchMode = iota

	// SearchSingleDir is when the finder skips other directories after there was a match
	SearchSingleDir
)

// NullInt is an integer that may not be defined.
type NullInt struct {
	Valid bool // Valid is set when the integer is defined
	Value int  // Value is the integer value
}

// DetectFunc is the type of a function that detects projects in a set of files
type DetectFunc func(filenames []string) []Project

// Finder finds supported projects
type Finder struct {
	Detect           DetectFunc        // Detect is the function used to detect projects in a directory
	FileTypes        []FileType        // FileTypes are the file types to be detected
	SearchMode       SearchMode        // SearchMode defines what should be skipped after there is match
	MaxDepth         NullInt           // MaxDepth is the maximum depth of directories being searched
	IgnoredDirs      []string          // IgnoredDirs are directories to be ignored when searching for projects
	IgnoreHiddenDirs bool              // IgnoreHiddenDirs is set to ignore directories whose name starts with ".".
	IsExcludedPath   func(string) bool // IsExcludedPath is a function that returns true when a file path is excluded.
}

// abortSearch is an error used to abort the search after detecting compatible projects
// in a directory, when search is limited to a single directory
var abortSearch = errors.New("abort search")

// FindProjects walks through a directory tree and search for compatible projects.
func (f Finder) FindProjects(root string) ([]Project, error) {
	projects, err := f.findProjects(root, ".", f.SearchMode, 0)
	switch err {
	case nil, abortSearch:
		return projects, nil
	default:
		return nil, err
	}
}

func (f Finder) findProjects(root, dir string, mode SearchMode, depth int) ([]Project, error) {
	log.Debugf("inspect directory: %s", dir)

	// read directory
	file, err := os.Open(filepath.Join(root, dir))
	if err != nil {
		return nil, err
	}
	infos, err := file.Readdir(-1)
	// close the directory right after reading it
	// to avoid opening its sub-directories while it's still open
	if closeErr := file.Close(); closeErr != nil {
		return nil, closeErr
	}
	if err != nil {
		return nil, err
	}

	// collect file and directory names
	dirnames := []string{}
	filenames := []string{}
	for _, info := range infos {
		// skip if path is excluded
		path := filepath.Join(dir, info.Name())
		if f.IsExcludedPath(path) {
			log.Debugf("skip excluded path: %s", path)
			continue
		}

		// add file or directory name
		if info.IsDir() {
			// skip directory if ignored
			if f.dirIsIgnored(info.Name()) {
				log.Debugf("skip ignored directory: %s", path)
				continue
			}
			dirnames = append(dirnames, info.Name())
			sort.Strings(dirnames)
		} else {
			filenames = append(filenames, info.Name())
			sort.Strings(filenames)
		}
	}

	// detect projects and set directory
	projects := f.Detect(filenames)
	for i := range f.Detect(filenames) {
		projects[i].Dir = dir
	}

	// abort if projects have been detected in directory,
	// and search is limited to a single directory
	if len(projects) > 0 && mode == SearchSingleDir {
		log.Infof("Detected supported dependency files in '%s'. Dependency files detected in this directory will be processed. Dependency files in other directories will be skipped.", projects[0].Dir)
		return projects, abortSearch
	}

	// skip directories if their depth is greater than maximum depth
	if f.MaxDepth.Valid && depth+1 > f.MaxDepth.Value {
		log.Debug("skip sub-directories")
		return projects, nil
	}

	// inspect sub-directories
	for _, dirname := range dirnames {
		subdir := filepath.Join(dir, dirname)
		subprojects, err := f.findProjects(root, subdir, mode, depth+1)
		if err != nil {
			// search might be aborted after finding compatible projects
			return subprojects, err
		}
		projects = append(projects, subprojects...)
	}

	return projects, nil
}

func (f Finder) dirIsIgnored(dir string) bool {
	if f.IgnoreHiddenDirs && strings.HasPrefix(dir, ".") {
		return true
	}
	for _, ignored := range f.IgnoredDirs {
		if dir == ignored {
			return true
		}
	}
	return false
}
