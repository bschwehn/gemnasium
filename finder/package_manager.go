package finder

// FileType is a type of file supported by package managers.
type FileType string

// PackageType is the package type.
type PackageType string

const (
	// FileTypeRequirements are the project requirements.
	// It lists the packages the project directly depends on,
	// and might also contain build instructions.
	FileTypeRequirements FileType = "requirements"

	// FileTypeLockFile is a lock file.
	// It lists all the packages depends on, including transient dependencies.
	// Most package managers an generate lock files, but build tools cannot.
	FileTypeLockFile FileType = "lockfile"

	// FileTypeGraphExport is an export of the dependency graph.
	FileTypeGraphExport FileType = "export"

	// PackageTypeConan is the type of Conan packages
	PackageTypeConan PackageType = "conan"

	// PackageTypeGem is the type of RubyGems
	PackageTypeGem PackageType = "gem"

	// PackageTypeMaven is the type of Maven artifacts (Java, Scala)
	PackageTypeMaven PackageType = "maven"

	// PackageTypeNpm is the type of npm packages
	PackageTypeNpm PackageType = "npm"

	// PackageTypeNuget is the type of NuGet packages
	PackageTypeNuget PackageType = "nuget"

	// PackageTypePackagist is the type of PHP Composer packages
	PackageTypePackagist PackageType = "packagist"

	// PackageTypePypi is the type of Python packages
	PackageTypePypi PackageType = "pypi"

	// PackageTypeGo is the type of Go packages
	PackageTypeGo PackageType = "go"
)

// PackageManager is a package manager or build tool that handles projects.
type PackageManager struct {
	// Name is a unique name
	Name string

	// PackageType is the type of package
	PackageType PackageType

	// Files are the files managed by the package manager
	Files []File
}

var (
	// PackageManagerConan describes Conan (C)
	PackageManagerConan = PackageManager{
		Name:        "conan",
		PackageType: PackageTypeConan,
		Files: []File{
			{Filename: "conanfile.txt", FileType: FileTypeRequirements},
			{Filename: "conanfile.py", FileType: FileTypeRequirements},
			{Filename: "conan.lock", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerComposer describes PHP Composer
	PackageManagerComposer = PackageManager{
		Name:        "composer",
		PackageType: PackageTypePackagist,
		Files: []File{
			{Filename: "composer.json", FileType: FileTypeRequirements},
			{Filename: "composer.lock", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerGo descrbies Go Modules
	PackageManagerGo = PackageManager{
		Name:        "go",
		PackageType: PackageTypeGo,
		Files: []File{
			{Filename: "go.mod", FileType: FileTypeRequirements},
			{Filename: "go.sum", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerNpm describes npm (JavaScript)
	PackageManagerNpm = PackageManager{
		Name:        "npm",
		PackageType: PackageTypeNpm,
		Files: []File{
			{Filename: "package.json", FileType: FileTypeRequirements},
			{Filename: "package-lock.json", FileType: FileTypeLockFile},
			{Filename: "npm-shrinkwrap.json", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerNuget describes Nuget
	PackageManagerNuget = PackageManager{
		Name:        "nuget",
		PackageType: PackageTypeNuget,
		Files: []File{
			{Filename: "*.csproj", FileType: FileTypeRequirements},
			{Filename: "packages.lock.json", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerYarn describes Yarn (JavaScript)
	PackageManagerYarn = PackageManager{
		Name:        "yarn",
		PackageType: PackageTypeNpm,
		Files: []File{
			{Filename: "package.json", FileType: FileTypeRequirements},
			{Filename: "yarn.lock", FileType: FileTypeLockFile},
			// TODO add yarn v2 lock files
			// See https://gitlab.com/gitlab-org/gitlab/-/issues/263358
		},
	}

	// PackageManagerBundler describes Bundler (Ruby)
	PackageManagerBundler = PackageManager{
		Name:        "bundler",
		PackageType: PackageTypeGem,
		Files: []File{
			{Filename: "Gemfile", FileType: FileTypeRequirements},
			{Filename: "gems.rb", FileType: FileTypeRequirements},
			{Filename: "Gemfile.lock", FileType: FileTypeLockFile},
			{Filename: "gems.locked", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerMaven describes Maven (Java)
	PackageManagerMaven = PackageManager{
		Name:        "maven",
		PackageType: PackageTypeMaven,
		Files: []File{
			{Filename: "pom.xml", FileType: FileTypeRequirements},
			{Filename: "gemnasium-maven-plugin.json", FileType: FileTypeGraphExport},
		},
	}

	// PackageManagerGradle describes Gradle (Java)
	PackageManagerGradle = PackageManager{
		Name:        "gradle",
		PackageType: PackageTypeMaven,
		Files: []File{
			{Filename: "build.gradle", FileType: FileTypeRequirements},
			{Filename: "build.gradle.kts", FileType: FileTypeRequirements},
			{Filename: "gradle-dependencies.json", FileType: FileTypeGraphExport},
		},
	}

	// PackageManagerSbt describes Sbt (Scala)
	PackageManagerSbt = PackageManager{
		Name:        "sbt",
		PackageType: PackageTypeMaven,
		Files: []File{
			{Filename: "build.sbt", FileType: FileTypeRequirements},
			{Filename: "dependencies-compile.dot", FileType: FileTypeGraphExport},
		},
	}

	// PackageManagerPipenv describes Pipenv (Python)
	PackageManagerPipenv = PackageManager{
		Name:        "pipenv",
		PackageType: PackageTypePypi,
		Files: []File{
			{Filename: "Pipfile", FileType: FileTypeRequirements},
			{Filename: "Pipfile.lock", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerPip describes Pip (Python)
	PackageManagerPip = PackageManager{
		Name:        "pip",
		PackageType: PackageTypePypi,
		Files: []File{
			// NOTE: filename defined by $PIP_REQUIREMENTS_FILE comes first
			{Filename: "requirements.txt", FileType: FileTypeRequirements},
			{Filename: "requirements.pip", FileType: FileTypeRequirements},
			{Filename: "requires.txt", FileType: FileTypeRequirements},
			{Filename: "pipdeptree.json", FileType: FileTypeGraphExport},
		},
	}

	// PackageManagerPoetry describes Poetry (Python)
	PackageManagerPoetry = PackageManager{
		Name:        "poetry",
		PackageType: PackageTypePypi,
		Files: []File{
			{Filename: "pyproject.toml", FileType: FileTypeRequirements},
			{Filename: "poetry.lock", FileType: FileTypeLockFile},
		},
	}

	// PackageManagerSetuptools describes Setuptools (Python)
	// This comes last becomes all Python projects might have a setup.py.
	PackageManagerSetuptools = PackageManager{
		Name:        "setuptools",
		PackageType: PackageTypePypi,
		Files: []File{
			{Filename: "setup.py", FileType: FileTypeRequirements},
			{Filename: "pipdeptree.json", FileType: FileTypeGraphExport},
		},
	}
)
