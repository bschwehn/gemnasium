package finder

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

func TestFinder(t *testing.T) {
	t.Run("FindProjects", func(t *testing.T) {
		t.Run("empty", func(t *testing.T) {
			finder := Finder{}
			_, err := finder.FindProjects("empty")
			require.Error(t, err)
		})

		// isNotExcluded is a placeholder that always returns false,
		// so that no path is excluded.
		isNotExcluded := func(string) bool {
			return false
		}

		var tcs = []struct {
			name   string
			dir    string
			config config
			want   []string
		}{

			// gemnasium
			{
				"gemnasium",
				"testdata/gemnasium",
				config{
					Preset:           PresetGemnasium,
					IgnoreHiddenDirs: true,
					IsExcludedPath:   isNotExcluded,
				},
				[]string{
					"bundler/gems.locked",
					"nested/bundler/Gemfile.lock",
					"nested/yarn/yarn.lock",
					"yarn/bundler/Gemfile.lock",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with max depth of 2
			{
				"gemnasium/max-depth/2",
				"testdata/gemnasium",
				config{
					Preset:         PresetGemnasium,
					MaxDepth:       NullInt{true, 2},
					IsExcludedPath: isNotExcluded,
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
					"nested/bundler/Gemfile.lock",
					"nested/yarn/yarn.lock",
					"yarn/bundler/Gemfile.lock",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with excluded files
			{
				"gemnasium/pathfilter",
				"testdata/gemnasium",
				config{
					Preset:   PresetGemnasium,
					MaxDepth: NullInt{true, 2},
					IsExcludedPath: func(path string) bool {
						switch filepath.Base(path) {
						case "Gemfile.lock", "yarn.lock":
							return true
						default:
							return false
						}
					},
				},
				[]string{
					"bundler/gems.locked",
				},
			},

			// gemnasium with excluded directories
			{
				"gemnasium/pathfilter",
				"testdata/gemnasium",
				config{
					Preset:   PresetGemnasium,
					MaxDepth: NullInt{true, 2},
					IsExcludedPath: func(path string) bool {
						return filepath.Base(path) == "bundler"
					},
				},
				[]string{
					"nested/yarn/yarn.lock",
					"yarn/yarn.lock",
					".vendor/Gemfile.lock",
				},
			},

			// gemnasium with max depth of 1
			{
				"gemnasium/max-depth/1",
				"testdata/gemnasium",
				config{
					Preset:         PresetGemnasium,
					MaxDepth:       NullInt{true, 1},
					IsExcludedPath: isNotExcluded,
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
					"yarn/yarn.lock",
				},
			},

			// gemnasium with max depth of 0
			{
				"gemnasium/max-depth/0",
				"testdata/gemnasium/yarn",
				config{
					Preset:         PresetGemnasium,
					MaxDepth:       NullInt{true, 0},
					IsExcludedPath: isNotExcluded,
				},
				[]string{
					"yarn.lock",
				},
			},

			// gemnasium with ignored dirs
			{
				"gemnasium/ignored-dirs",
				"testdata/gemnasium",
				config{
					Preset:           PresetGemnasium,
					IgnoredDirs:      []string{"nested", "yarn"},
					IgnoreHiddenDirs: false,
					IsExcludedPath:   isNotExcluded,
				},
				[]string{
					".vendor/Gemfile.lock",
					"bundler/gems.locked",
				},
			},

			// gemnasium-maven
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset:         PresetGemnasiumMaven,
					IsExcludedPath: isNotExcluded,
				},
				[]string{
					"app3/pom.xml",
				},
			},

			// gemnasium-maven with excluded directory
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset: PresetGemnasiumMaven,
					IsExcludedPath: func(path string) bool {
						return filepath.Base(path) == "app3"
					},
				},
				[]string{
					"app4/pom.xml",
				},
			},

			// gemnasium-maven with excluded path
			{
				"maven",
				"testdata/gemnasium-maven",
				config{
					Preset: PresetGemnasiumMaven,
					IsExcludedPath: func(path string) bool {
						return path == "app3/pom.xml"
					},
				},
				[]string{
					"app3/build.gradle",
				},
			},

			// gemnasium-python
			{
				"gemnasium-python",
				"testdata/gemnasium-python",
				config{
					Preset:         PresetGemnasiumPython,
					IsExcludedPath: isNotExcluded,
				},
				[]string{
					"app/requirements.txt",
				},
			},

			// gemnasium-python with pip requirements filename
			{
				"gemnasium-python/pip-requirements-file",
				"testdata/gemnasium-python/app/server",
				config{
					Preset:              PresetGemnasiumPython,
					PipRequirementsFile: "requirements-production.txt",
					IsExcludedPath:      isNotExcluded,
				},
				[]string{
					"requirements-production.txt",
					"requirements.txt",
				},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				// find projects
				finder := newFinder(tc.config)
				projects, err := finder.FindProjects(tc.dir)
				require.NoError(t, err)

				// extract, sort file paths
				got := []string{}
				for _, project := range projects {
					for _, file := range project.Files {
						got = append(got, project.FilePath(file))
					}
				}

				// compare sorted paths
				require.ElementsMatch(t, tc.want, got)
			})
		}
	})
}
