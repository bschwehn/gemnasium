require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

describe "running image" do
  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "outputs analyzer version" do
      analyzer_version = "2.32.0"
      expect(@output).to match(/GitLab Gemnasium analyzer v#{analyzer_version}/i)
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    def fixtures_dir
      File.expand_path("../qa/fixtures", __dir__)
    end

    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      base_dir = File.expand_path("../qa/expect", __dir__)
      path = File.join(base_dir, expectation_name, report_filename)
      JSON.parse File.read path
    end

    let(:global_vars) do
      { "GEMNASIUM_DB_REF_NAME": "v1.2.142" }
    end

    let(:project) { "any" }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }

    let(:scan) do
      target_dir = File.join(fixtures_dir, project)
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, target_dir,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    context "containing multiple sub-projects" do
      let(:project) { "multi-project/main" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "report with scanned files", ["go.sum", "Gemfile.lock", "composer.lock"]

        it_behaves_like "valid report"

        describe "vulnerabilities" do
          subject(:actual_vulns) { GitlabSecure::IntegrationTest::Comparable.vulnerabilities(report["vulnerabilities"]) }

          ["go-modules", "ruby-bundler", "php-composer"].each do |project_name|
            it "includes vulnerabilites for #{project_name}" do
              expected_report = parse_expected_report project_name
              expected_vulns = GitlabSecure::IntegrationTest::Comparable.vulnerabilities(expected_report["vulnerabilities"])

              expect(actual_vulns).to include(*expected_vulns)
            end
          end
        end
      end
    end

    context "with csharp-nuget-dotnetcore" do
      let(:project) { "csharp-nuget-dotnetcore/main" }

      context "when all dependencies have a dependency path" do
        let(:variables) do
          { DS_DEPENDENCY_PATH_MODE: "all" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["src/web.api/packages.lock.json"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "csharp-nuget-dotnetcore" }
          end

          it_behaves_like "valid report"
        end
      end
    end

    context "with c-conan" do
      let(:project) { "c-conan/main" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "report with scanned files", ["conan.lock"]

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report "c-conan" }
        end

        it_behaves_like "valid report"
      end
    end

    context "with go-modules" do
      let(:project) { "go-modules/main" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "report with scanned files", ["go.sum"]

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report "go-modules" }
        end

        it_behaves_like "valid report"
      end

      context "when offline" do
        let(:offline) { true }

        context "when gemnasium-db update disabled" do
          let(:variables) do
            { GEMNASIUM_DB_UPDATE_DISABLED: "true" }
          end

          it_behaves_like "successful scan"
        end

        context "when gemnasium-db update NOT disabled" do
          it_behaves_like "crashed scan"
        end
      end

      context "with clone of gemnasium-db" do
        let(:project) { "go-modules/advisory-db-scan-time-sync" }

        let(:gemnasium_db_ref_name) { "master" }

        let(:clone_config) do
          # config for setup_local_remote.sh
          {
            ORIGINAL_REMOTE_STABLE_BRANCH: "2020-03-13",
            ORIGINAL_REMOTE_OLDER_STABLE_BRANCH: "2020-01-15",
            NEW_BRANCH_NAME: "new-branch-foobarbaz",
            NEW_TAG_NAME: "new-tag-foobarbaz"
          }
        end

        let(:variables) do
          {
            GEMNASIUM_DB_REF_NAME: gemnasium_db_ref_name,
            GEMNASIUM_DB_REMOTE_URL: "/gemnasium-db-local-remote"
          }.merge(clone_config)
        end

        let(:script) do
          setup_script_path = File.expand_path("../qa/scripts/setup_local_remote.sh", __dir__)
          setup_script = File.read(setup_script_path)
          <<-HERE
#!/bin/sh
set -ex
git config --global user.email "you@example.com"

# setup_local_remote.sh
#{setup_script}

/analyzer run
          HERE
        end

        describe "when requested git ref is a branch that has been updated remotely" do
          let(:gemnasium_db_ref_name) { clone_config[:ORIGINAL_REMOTE_STABLE_BRANCH] }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "branch-updated-report.json"
              end
            end
          end
        end

        describe "when requested git ref is a branch that has NOT been updated remotely" do
          let(:gemnasium_db_ref_name) { clone_config[:ORIGINAL_REMOTE_OLDER_STABLE_BRANCH] }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "old-branch-checkout-report.json"
              end
            end
          end
        end

        describe "when requested git ref is a branch that only exists remotely" do
          let(:gemnasium_db_ref_name) { clone_config[:NEW_BRANCH_NAME] }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "new-branch-checkout-report.json"
              end
            end
          end
        end

        describe "when requested git ref is a tag that exists locally" do
          let(:gemnasium_db_ref_name) { "v1.0.199" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "old-tag-checkout-report.json"
              end
            end
          end
        end

        describe "when requested git ref is a tag that only exists remotely" do
          let(:gemnasium_db_ref_name) { clone_config[:NEW_TAG_NAME] }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "new-tag-checkout-report.json"
              end
            end
          end
        end

        describe "when requested git ref is a commit SHA" do
          let(:gemnasium_db_ref_name) { "26c90d4a2dd9901bd9c18fc809f9c692f56e2979" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report" do
              let(:recorded_report) do
                parse_expected_report "go-modules/advisory-db-scan-time-sync",
                                      report_filename: "checkout-by-commit-sha-report.json"
              end
            end
          end
        end
      end

      context "when excluding go.sum with DS_EXCLUDED_PATHS" do
        let(:variables) { { "DS_EXCLUDED_PATHS": "/go.sum" } }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "empty report"
          it_behaves_like "valid report"
        end
      end

      context "when in a subdirectory" do
        let(:project) { "go-modules/subdir" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["subdir/go.sum"]

          it_behaves_like "valid report"
        end
      end
    end

    context "with js-npm" do
      context "with lockfile v1" do
        let(:project) { "js-npm/main" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["package-lock.json"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "js-npm" }
          end

          it_behaves_like "valid report"
        end
      end

      context "with minified jquery" do
        let(:project) { "js-npm/minified-jquery" }

        context "when not enabling scan of libraries" do
          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"
            it_behaves_like "report with scanned files", ["package-lock.json"]

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report "js-npm" }
            end

            it_behaves_like "valid report"
          end
        end

        context "when enabling scan of libraries" do
          let(:variables) { { "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true" } }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            describe "vulnerable files" do
              specify do
                skip "report is missing" if report.nil?

                vuln_files = report["vulnerabilities"].map { |v| v.dig("location", "file") }.uniq.sort
                expect(vuln_files).to eql [
                  "node_modules/await-to-js/dist/docs/assets/js/main.js",
                  "node_modules/jed/test/jquery.min.js",
                  "node_modules/JSONSelect/site/js/jquery-1.6.1.min.js",
                  "node_modules/JSONSelect/src/test/js/jquery-1.6.1.min.js",
                  "package-lock.json"
                ].sort
              end
            end

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report "js-npm/minified-jquery" }
            end

            it_behaves_like "valid report"
          end
        end

        context "when enabling scan of libraries but retire fails" do
          let(:variables) {
            {
              "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true",
              "GEMNASIUM_RETIREJS_JS_ADVISORY_DB": "/missing",
            }
          }

          it_behaves_like "failed scan"

          it "logs why retire has failed" do
            expect(scan.combined_output).to match /no such file or directory/
          end
        end

        context "when enabling scan of libs but excluding JSONSelect" do
          let(:variables) {
            {
              "GEMNASIUM_LIBRARY_SCAN_ENABLED": "true",
              "DS_EXCLUDED_PATHS": "JSONSelect",
            }
          }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "non-empty report"

            describe "vulnerable files" do
              specify do
                skip "report is missing" if report.nil?

                vuln_files = report["vulnerabilities"].map { |v| v.dig("location", "file") }.uniq.sort
                expect(vuln_files).to eql [
                  "node_modules/await-to-js/dist/docs/assets/js/main.js",
                  "node_modules/jed/test/jquery.min.js",
                  "package-lock.json"
                ].sort
              end
            end
          end
        end
      end

      context "with lockfile v2" do
        let(:project) { "js-npm/lockfileVersion2" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["package-lock.json"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "js-npm" }
          end

          it_behaves_like "valid report"
        end
      end

      context "with shrinkwrap file" do
        let(:project) { "js-npm/shrinkwrap" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["npm-shrinkwrap.json"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "js-npm-shrinkwrap" }
          end

          it_behaves_like "valid report"
        end
      end
    end

    context "with js-yarn" do
      context "when remediation is disabled" do
        let(:project) { "js-yarn/main" }

        let(:variables) do
          { DS_REMEDIATE: "false" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["yarn.lock"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "js-yarn" }
          end

          it_behaves_like "valid report"
        end
      end

      context "when remediation is enabled" do
        let(:project) { "js-yarn/remediate-top-level" }

        let(:variables) do
          { DS_REMEDIATE: "true" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["yarn.lock"]

          # NOTE: The generated report does not contain the expected remediations
          # because auto-remediation requires a valid git directory.
          pending "recorded report"

          it_behaves_like "valid report"
        end
      end
    end

    context "with php-composer" do
      let(:project) { "php-composer/main" }

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"
        it_behaves_like "report with scanned files", ["composer.lock"]

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report "php-composer" }
        end

        it_behaves_like "valid report"
      end

      context "with broken composer.lock" do
        let(:project) { "php-composer/broken" }

        it_behaves_like "failed scan"

        context "but the broken file is excluded" do
          let(:variables) { { "DS_EXCLUDED_PATHS": "composer.lock" } }

          it_behaves_like "successful scan"
        end
      end
    end

    context "with ruby-bundler" do
      context "with Gemfile and Gemfile.lock" do
        let(:project) { "ruby-bundler/main" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Gemfile.lock"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report "ruby-bundler" }
          end

          it_behaves_like "valid report"
        end

        context "with a vulnerability database where advisories have multiple identifiers" do
          let(:variables) do
            # switch to a version of the vulnerability database where some advisories
            # have multiple identifiers in the "identifiers" YAML field;
            # see CVE-2021-41098 (nokogiri) for instance
            { "GEMNASIUM_DB_REF_NAME": "v2.0.481" }
          end

          describe "created report" do
            it_behaves_like "non-empty report"
            it_behaves_like "report with scanned files", ["Gemfile.lock"]

            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report "ruby-bundler/gemnasium-db-v2.0.481" }
            end

            it_behaves_like "valid report"
          end
        end
      end

      context "when gems.rb and gems.locked" do
        let(:project) { "ruby-bundler/with-gems.locked" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["gems.locked"]

          it_behaves_like "valid report"
        end
      end
    end
  end
end
