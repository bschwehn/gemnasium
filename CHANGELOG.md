# Gemnasium analyzer changelog

## v2.32.0
- Scan JavaScript vendored libraries using `retire` when `GEMNASIUM_LIBRARY_SCAN_ENABLED` is true (!266)

## v2.31.0
- Report vulnerabilities with all the identifiers available in the vulnerability database (!258)

## v2.30.5
- Remove root project and sub-projects from dependency lists of Sbt builds (!260)

## v2.30.4
- Skip self-edges to prevent `panic: simple: adding self edge [recovered]` errors (!255)

## v2.30.3
- Remove `PIP_EXTRA_INDEX_URL` from the log (!253)

## v2.30.2
- Fix a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!252)

## v2.30.1
- Show INFO message when a single directory is scanned, and other directories are skipped (!247)

## v2.30.0
- Use `DS_EXCLUDED_PATHS` variable to filter out paths prior to the scan (!248)

## v2.29.11
- Upgrade to Go 1.17 (!239)

## v2.29.10
- Use detected package manager in `package_manager` field of JSON reports. (!235)

## v2.29.9
- Ensure files and directories are sorted when searching for compatible projects (!227)

## v2.29.8
- Fix a bug which causes a panic when a node collision occurs while building the dependency tree (!131)

## v2.29.7
- Fix non-determinstic result when parsing a NuGet lock file having multiple targets (!197)

## v2.29.6
- Update to Security Report Schema `v14.0.0` (!188)

## v2.29.5
- Fix ignored `PIP_REQUIREMENTS_FILE` variable (!187)

## v2.29.4
- Change pipenv builder to try to install using the version of Python defined in the virtualenv (!186)

## v2.29.3
- Replace environment variable `GEMNASIUM_DB_UPDATE` with `GEMNASIUM_DB_UPDATE_DISABLED` (!185)

## v2.29.2
- Fix "X is a directory" error in output log (!183)

## v2.29.1
- Change dependency graph export file name for sbt projects (!182)

## v2.29.0
- Add parser for DOT files created by sbt-dependency-graph (!171)
- Change permissions for Red Hat OpenShift compatibility (!180)

## v2.28.2
- Fix vulnerability database update so that it can sync tags, branches, and commit refs (!179)

## v2.28.1
- Fix vulnerability database updates when the git ref points to a branch (!162)
- Log git commit of the vulnerability database (!162)

## v2.28.0
- Enforce max depth for directories being searched, default to 2 (!159)

## v2.27.0
- Support lock file format v2 introduced in npm 7 (!164)

## v2.26.2
- Fix yarn parsing error when version spec empty (!160)

## v2.26.1
- Update go-cvss to v0.4.0 to remediate CVE-2020-14040 (!169)

## v2.26.0
- Update Node to version 14 (LTS) and Alpine to 3.12 (!166)

## v2.25.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!157)

## v2.25.0
- Skip hidden directories by default (!156)

## v2.24.1
- Skip auto-remediation when yarn lock file hasn't changed (!147)
- Improve log messages for auto-remediation of yarn projects(!147)

## v2.24.0
- Detect supported projects, and scan no more than one project per package type and directory (!134)

## v2.23.0
- Add `find` cli command show the list of files the analyzer would scan (!138)

## v2.22.0
- Update Go dependencies `common` and `urfave/cli` to remediate vulnerability GMS-2019-2 (!129)

## v2.21.0
- Extract dependency graph information from yarn v1 lock files (!121)

## v2.20.0
- Only report dependency path to vulnerable dependency (!117)

## v2.19.0
- Match security advisories against Go pseudo-versions (!115)

## v2.18.1
- Warn if no files match instead of returning error (!109)

## v2.18.0
- Add support for extracting dependency links when parsing dependency files (!101, !102)
- Extract dependency links when parsing NuGet lock files (!103)
- Add `dependency_path` and dependency `iid` to report when dependency file parsers can extract dependency links (!105)

## v2.17.1
- Add `apk upgrade` command to `Dockerfile` to ensure that all installed packages are recent (!106)
- Upgrade to musl 1.1.20-r5, musl-utils 1.1.20-r5, libcrypto1.1 1.1.1g-r0, libssl1.1 1.1.1g-r0, ca-certificates-cacert and 20191127-r2 (!106)

## v2.17.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!99)

## v2.16.0
- Add support for parsing and scanning Conan lock files (!98)

## v2.15.0
- Update common to `v2.14.0` which allows git to use CA Certificate bundle (!97)

## v2.14.0
- Add scan object to report (!95)

## v2.13.0
- Add support for parsing and scanning NuGet lock files (!87)

## v2.12.1
- Do not fail because of missing advisories for a package type that is not scanned (!91)

## v2.12.0
- Add dependency file parser for `poetry.lock` (@janw) (!66)

## v2.11.0
- Update logging to be standardized across analyzers (!82)

## v2.10.1
- Fix link to advisory (!80)

## v2.10.0
- Output Severity value for vulnerabilities (!75)

## v2.9.0
- Add `id` JSON field to vulnerabilities (!64)

## v2.8.1
- Add ability to checkout commit refs of `gemnasium-db` (!73)

## v2.8.0
- Add support for custom CA certs (!63)

## v2.7.1
- Don't trim leading `v` in version number of Go dependencies
  when adding to the dependency list and comparing to advisories (!62)

## v2.7.0
- Add support for scanning and parsing `go` dependencies (!57)

## v2.6.0
- Add support for parsing ivy dependency reports (!54)

## v2.5.0
- Add support for scanning and parsing gradle dependencies (!53)

## v2.4.0
- Match python package advisories as per PEP426 (!52)

## v2.3.0
- Use gemnasium-db git repo instead of the Gemnasium API (!25)

## v2.2.6
- Fix The engine "node" is incompatible with this module. error (!22)

## v2.2.5
- Remove duplicate from npm dependencies (!21)

## v2.2.4
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!18)

## v2.2.3
- Fix dependency list, include dependency files which do not have any vulnerabilities (!17)

## v2.2.2
- Fix npm-shrinkwrap.json files not parsed (!12)

## v2.2.1
- Sort the dependency files and their dependencies (!14)
- Fix vulnerabilities not sorted in report (!14)
- Fix missing `DS_EXCLUDED_PATH` variable (!14)

## v2.2.0
- List the dependency files and their dependencies (!13)

## v2.1.2
- Update common to v2.1.6

## v2.1.1
- Sort vulnerability.links to ensure stable order

## v2.1.0
- Implement vulnerabilities remediation for yarn

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
