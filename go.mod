module gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/ProtonMail/go-crypto v0.0.0-20211221144345-a4f6767435ab // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spiegel-im-spiegel/go-cvss v0.4.0
	github.com/stretchr/testify v1.7.0
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.7.1
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce // indirect
	golang.org/x/net v0.0.0-20220111093109-d55c255bac03 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	gonum.org/v1/gonum v0.8.1
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

go 1.13
