const semver = require('semver');

module.exports = {
    processDocument(item) {
        try {
            if (!semver.validRange(item.range, true)) {
                throw new Error('malformed range string');
            }
            if (!semver.valid(item.version, true)) {
                throw new Error('malformed version string');
            }
            const satisfies = semver.satisfies(item.version, item.range, {
                includePrerelease: true, loose: true
            });
            return {'range': item.range, 'version': item.version, satisfies};
        } catch (e) {
            return {'range': item.range, 'version': item.version, 'error': e.message};
        }
    }
}