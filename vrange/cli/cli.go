package cli

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

// Register registers a CLI-based resolver.
func Register(syntaxName, path string, args ...string) {
	r := &Resolver{
		Name: syntaxName,
		Path: path,
		Args: args,
	}
	vrange.Register(syntaxName, r)
}

// Resolver is a CLI-based resolver.
type Resolver struct {
	Name string   // Name is the name used to configure the resolver.
	Path string   // Path is the path of the command to run.
	Args []string // Args are the command arguments.
}

// Flags returns the CLI flags that configure the resolver.
func (r Resolver) Flags() []cli.Flag {
	usage := fmt.Sprintf("Path to CLI command that evaluates version range for %s", r.Name)
	envVar := fmt.Sprintf("VRANGE_%s_CMD", strings.ToUpper(r.Name))
	value := r.Path
	if len(r.Args) != 0 {
		value += " " + strings.Join(r.Args, " ")
	}

	return []cli.Flag{
		&cli.StringFlag{
			Name:    r.cmdFlag(),
			Usage:   usage,
			EnvVars: []string{envVar},
			Value:   value,
		},
	}
}

// cmdFlag returns the name of the CLI flag that configures the CLI.
func (r Resolver) cmdFlag() string {
	return fmt.Sprintf("vrange-%s-cmd", strings.ToLower(r.Name))
}

// Configure configures the CLI-based resolver with CLI context and options,
// and ensures that the command exists.
func (r *Resolver) Configure(c *cli.Context, opts vrange.Options) error {
	cmd := c.String(r.cmdFlag())
	slice := strings.SplitN(cmd, " ", 2)
	path := filepath.Join(opts.BaseDir, slice[0])
	if _, err := os.Stat(path); err != nil {
		return err
	}
	r.Path = path
	r.Args = slice[1:]
	return nil
}

// Resolve evaluates queries.
func (r Resolver) Resolve(queries []vrange.Query) (*vrange.ResultSet, error) {

	log.Debugf("%s", "In cli.go, start Resolve")
	// create input document
	tmpfile, err := ioutil.TempFile("/tmp", "vrange_queries")
	if err != nil {
		return nil, err
	}
	defer os.Remove(tmpfile.Name())
	if err := json.NewEncoder(tmpfile).Encode(queries); err != nil {
		return nil, err
	}
	if err := tmpfile.Close(); err != nil {
		return nil, err
	}

	log.Debugf("In cli.go, Resolve with path: %s and args: %s", r.Path, r.Args)
	log.Debugf("queryfile: %s", tmpfile.Name())

	f, err := os.Open(tmpfile.Name())
	defer f.Close()

	buf := new(strings.Builder)
	_, err = io.Copy(buf, f)
	log.Debugf("in cli.go, vrange query file content: %s", buf.String())

	// run command, read output
	args := append(r.Args, tmpfile.Name())
	cmd := exec.Command(r.Path, args...)
	out, err := cmd.CombinedOutput()
	log.Debugf("In cli.go, exec with out: %s and err: %s", out, err)
	if err != nil {
		return nil, err
	}

	// decode results
	results := []struct {
		Version   string
		Range     string
		Satisfies bool
		Error     string
	}{}
	log.Debugf("in cli.go, before Unmarshal")
	if err := json.Unmarshal(out, &results); err != nil {
		return nil, err
	}

	log.Debugf("in cli.go, after Unmarshal")
	// build result set
	set := make(vrange.ResultSet)
	for _, result := range results {
		log.Debugf("in cli.go, building results with result: %s", result)
		var err error = nil
		if result.Error != "" {
			err = errors.New(result.Error)
		}
		query := vrange.Query{Version: result.Version, Range: result.Range}
		set.Set(query, result.Satisfies, err)
	}
	log.Debugf("in cli.go, return")
	return &set, nil
}
