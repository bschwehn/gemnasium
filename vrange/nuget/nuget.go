package nuget

import (
	"runtime"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/cli"
)

func init() {
	cli.Register("nuget", "nuget/vrange-"+runtime.GOOS)
}
