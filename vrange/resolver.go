package vrange

import (
	"github.com/urfave/cli/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
)

// Resolver is implemented by types that can evaluate queries.
type Resolver interface {
	Resolve([]Query) (*ResultSet, error) // Resolve evaluates queries
}

// NewResolver returns a resolver matching the given name.
func NewResolver(name string) (Resolver, error) {
	resolver := Lookup(name)
	log.Debugf("%s %s", "In resolver.go, after lookup with name:", name)
	if resolver == nil {
		return nil, ErrResolverNotFound{name, Resolvers()}
	}
	return resolver, nil
}

// Options are options for configurable resolvers
type Options struct {
	BaseDir string // BaseDir is the base directory for this Go package
}

// Configurable is implemented by resolvers that can be configured using CLI flags
type Configurable interface {
	Flags() []cli.Flag                     // Flags returns the CLI flags that configure the resolver
	Configure(*cli.Context, Options) error // Configure configures the resolver with CLI context and options
}

// QueryTranslator is implemented by types that can translate queries
// using version metadata.
type QueryTranslator interface {
	TranslateQuery(Query, []advisory.Version) Query
}
