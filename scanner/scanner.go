package scanner

import (
	"fmt"
	"os"
	"path/filepath"
	"io"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/libfinder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	flagPrefix           = "gemnasium-"
	flagDBUpdateDisabled = flagPrefix + "db-update-disabled"
	flagDBLocalPath      = flagPrefix + "db-local-path"
	flagDBRemoteURL      = flagPrefix + "db-remote-url"
	flagDBWebURL         = flagPrefix + "db-web-url"
	flagDBRefName        = flagPrefix + "db-ref-name"

	envVarPrefix           = "GEMNASIUM_"
	envVarDBUpdateDisabled = envVarPrefix + "DB_UPDATE_DISABLED"
	envVarDBLocalPath      = envVarPrefix + "DB_LOCAL_PATH"
	envVarDBRemoteURL      = envVarPrefix + "DB_REMOTE_URL"
	envVarDBWebURL         = envVarPrefix + "DB_WEB_URL"
	envVarDBRefName        = envVarPrefix + "DB_REF_NAME"
)

// Flags generates new command line flags for the scanner
func Flags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagDBUpdateDisabled,
			EnvVars: []string{envVarDBUpdateDisabled},
			Usage:   "Disable gemnasium-db git repo update before scanning",
			Value:   false,
		},
		&cli.StringFlag{
			Name:    flagDBLocalPath,
			EnvVars: []string{envVarDBLocalPath},
			Usage:   "Path of gemnasium-db git repo",
			Value:   "gemnasium-db",
		},
		&cli.StringFlag{
			Name:    flagDBRemoteURL,
			EnvVars: []string{envVarDBRemoteURL},
			Usage:   "Remote URL the local gemnasium-db git repo is synced with",
		},
		&cli.StringFlag{
			Name:    flagDBWebURL,
			EnvVars: []string{envVarDBWebURL},
			Usage:   "Web URL of the gemnasium-db GitLab project",
		},
		&cli.StringFlag{
			Name:    flagDBRefName,
			EnvVars: []string{envVarDBRefName},
			Usage:   "git reference the local gemnasium-db git repo is synced with",
		},
	}
}

// NewScanner parses command line arguments from a cli.Context and returns a new scanner
func NewScanner(c *cli.Context) (*Scanner, error) {
	// GitLab project and git repo for the advisories
	r := advisory.Repo{
		Path:      c.String(flagDBLocalPath),
		RemoteURL: c.String(flagDBRemoteURL),
		WebURL:    c.String(flagDBWebURL),
		RefName:   c.String(flagDBRefName),
	}

	// update advisory repo if requested
	if !c.Bool(flagDBUpdateDisabled) {
		if err := r.Update(); err != nil {
			return nil, err
		}
	}

	return &Scanner{r}, nil
}

// Scanner is a project scanner
type Scanner struct {
	repo advisory.Repo
}

// ScanProjects scans projects and returns a description of each file it has scanned.
// This includes the dependencies found in the file, and the vulnerabilities affecting them.
func (s Scanner) ScanProjects(dir string, projects []finder.Project) ([]File, error) {
	result := []File{}
	for _, project := range projects {
		log.Debugf("%s", "In scanner.go, range projects")
		scannable, isScannable := project.ScannableFile()
		if !isScannable {
			// skip project
			log.Debugf("skip project: no scannable file found in %s", project.Dir)
			continue
		}

		// relative and absolute path of scannable file
		rel := project.FilePath(scannable)
		path := filepath.Join(dir, rel)

		// set location to relative path of scannable file
		location := rel
		if !scannable.Linkable() {
			// update location with the relative path of the requirements file
			if requirements, ok := project.RequirementsFile(); ok {
				location = project.FilePath(requirements)
			}
		}

		// scan dependency file
		file := File{
			RootDir:        dir,
			Path:           location,
			PackageManager: project.PackageManager.Name,
		}
		if err := s.scanFile(path, &file); err != nil {
			return nil, err
		}
		log.Debugf("%s", "In scanner.go, before append")
		result = append(result, file)
		log.Debugf("%s", "In scanner.go, after append")
	}

	log.Debugf("%s", "In scanner.go, end of ScanProjects")

	return result, nil
}

// ScanLibs scans each vendored library and returns files
// that combine a file path with vulnerabilities.
func (s Scanner) ScanLibs(dir string, libs []libfinder.Library) ([]File, error) {
	result := []File{}
	for _, lib := range libs {
		pkgs := []parser.Package{lib.Package}
		pkgType := string(lib.PackageType)

		// convert to scanner file
		file := File{
			RootDir:     dir,
			Path:        lib.Path,
			PackageType: pkgType,
			Packages:    pkgs,
		}

		// find affections
		aff, err := pkgAffections(pkgs, pkgType, s.repo)
		if err != nil {
			return nil, err
		}
		file.Affections = aff

		result = append(result, file)
	}

	return result, nil
}

// scanFile opens, parses, and scans a dependency file with a given path.
// It updates the packages, dependencies, package type, and affections
// of the given File struct.
func (s Scanner) scanFile(path string, file *File) error {
	// open file
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	buf := new(strings.Builder)
	_, err = io.Copy(buf, f)
	log.Debugf("%s, %s", "Doc: ", buf.String())
	f.Seek(0, io.SeekStart)
	// find file parser
	basename := filepath.Base(path)
	depParser := parser.Lookup(basename)
	if depParser == nil {
		return fmt.Errorf("no file parser for: %s", path)
	}
	pkgType := string(depParser.PackageType)
	file.PackageType = pkgType

	// parse file
	pkgs, deps, err := depParser.Parse(f)
	if err != nil {
		return err
	}
	log.Debugf("%s", "In scanner.go, after Parse")
	file.Packages = pkgs
	file.Dependencies = deps

	// find affections
	log.Debugf("%s", "In scanner.go, before pkgAffections")
	aff, err := pkgAffections(pkgs, pkgType, s.repo)
	log.Debugf("%s", "In scanner.go, after pkgAffections")
	if err != nil {
		return err
	}
	file.Affections = aff

	log.Debugf("%s", "In scanner.go, after Affections")
	return nil
}

type queryTranslator func(vrange.Query, []advisory.Version) vrange.Query

// pkgAffections returns affections for packages of a given type.
//
// For each package, this functions finds the security advisories matching the package type and name,
// then filters out the security advisories that don't match the package version.
// The function returns affections;
// an affection combines a package with a security advisory affecting that package.
func pkgAffections(pkgs []parser.Package, pkgType string, repo advisory.Repo) ([]Affection, error) {
	// version range resolver
	log.Debugf("%s", "In scanner.go, before newResolver")
	resolver, err := vrange.NewResolver(pkgTypeToResolverName(pkgType))
	log.Debugf("%s", "In scanner.go, after newResolver")
	if err != nil {
		return nil, err
	}

	// query translator
	var translateQuery queryTranslator
	switch t := resolver.(type) {
	default:
		// use identify function
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return q
		}
	case vrange.QueryTranslator:
		// delegate to resolver
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return t.TranslateQuery(q, versionMeta)
		}
	}

	// ensure repo has advisories matching the package type
	if err := repo.SatisfyPackageType(pkgType); err != nil {
		return nil, err
	}
	log.Debugf("%s", "In scanner.go, after SatisfyPackageType")

	// collect possible affections, list version range queries
	affections := []Affection{}
	queries := []vrange.Query{}
	for _, pkg := range pkgs {

		// list package advisories
		apkg := advisory.Package{Type: pkgType, Name: pkg.Name}
		paths, err := repo.PackageAdvisories(apkg)
		if _, ok := err.(advisory.ErrNoPackageDir); ok {
			continue // no advisories
		}
		if err != nil {
			return nil, err
		}

		// fetch advisories, append affections and queries
		for _, path := range paths {
			adv, err := repo.Advisory(path)
			if err != nil {
				return nil, err
			}
			aff := Affection{pkg, *adv}
			affections = append(affections, aff)
			q := translateQuery(aff.query(), aff.Advisory.Versions)
			queries = append(queries, q)

		}
	}

	// resolve queries and tell if dependency version is in affected range
	log.Debugf("%s", "In scanner.go, before resolver.resolve")
	result, err := resolver.Resolve(queries)
	if err != nil {
		return nil, err
	}
	log.Debugf("%s", "In scanner.go, after resolver.resolve")
	// filter affections and keep those where the dependency version is in affected range
	filtered := []Affection{}
	for _, aff := range affections {
		log.Debugf("%s", "In scanner.go, range affections")
		q := translateQuery(aff.query(), aff.Advisory.Versions)
		ok, err := result.Satisfies(q)
		if err != nil {
			// TODO report error
			continue
		}
		if ok {
			filtered = append(filtered, aff)
		}
	}
	return filtered, nil
}

// pkgTypeToResolverName converts a package type to the name of a vrange resolver.
func pkgTypeToResolverName(pkgType string) string {
	switch pkgType {
	case "npm", "maven", "gem", "go":
		return pkgType
	case "packagist":
		return "php"
	case "pypi":
		return "python"
	default:
		return pkgType
	}
}
