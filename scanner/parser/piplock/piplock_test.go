package piplock

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestPipLock(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_version", "Pipfile.lock")
			_, _, err := Parse(fixture)
			require.EqualError(t, err, parser.ErrWrongFileFormat.Error())
		})

		for _, tc := range []string{"simple", "big", "old"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "Pipfile.lock")
				got, _, err := Parse(fixture)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, got)
			})
		}
	})
}
