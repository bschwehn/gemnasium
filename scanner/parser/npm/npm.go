package npm

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Parse scans a npm lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	lockfile := Lockfile{}
	err := json.NewDecoder(r).Decode(&lockfile)
	pkgs, err := lockfile.Parse()
	return pkgs, nil, err
}

func init() {
	parser.Register("npm", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeNpm,
		Filenames:   []string{"package-lock.json", "npm-shrinkwrap.json"},
	})
}
