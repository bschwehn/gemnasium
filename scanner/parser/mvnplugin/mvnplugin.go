package mvnplugin

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Dependency is used to decode a project dependency from output
type Dependency struct {
	GroupID    string `json:"groupId"`
	ArtifactID string `json:"artifactId"`
	Version    string `json:"version"` // Installed version
}

// Parse scans the output of the Gemnasium Maven plugin and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	deps := []Dependency{}
	err := json.NewDecoder(r).Decode(&deps)
	if err != nil {
		return nil, nil, err
	}
	result := make([]parser.Package, len(deps))
	for i, dep := range deps {
		name := dep.GroupID + "/" + dep.ArtifactID
		result[i] = parser.Package{Name: name, Version: dep.Version}
	}
	return result, nil, nil
}

func init() {
	parser.Register("mvnplugin", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeMaven,
		Filenames:   []string{"maven-dependencies.json", "gemnasium-maven-plugin.json", "gradle-dependencies.json"},
	})
}
