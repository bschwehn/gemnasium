package poetry

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestNpm(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"missing_content_hash", "missing_python_versions"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "poetry.lock")
				_, _, err := Parse(fixture)
				require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
			})
		}

		for _, tc := range []string{"simple", "big"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "poetry.lock")
				got, _, err := Parse(fixture)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, got)
			})
		}
	})
}
