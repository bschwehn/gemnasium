package nuget

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestNuget(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_version", "packages.lock.json")
			_, _, err := Parse(fixture)
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		for _, tc := range []string{"web.api", "duplicates"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "packages.lock.json")
				pkgs, deps, err := Parse(fixture)
				require.NoError(t, err)

				t.Run("packages", func(t *testing.T) {
					testutil.RequireExpectedPackages(t, tc, pkgs)
				})

				t.Run("dependencies", func(t *testing.T) {
					testutil.RequireExpectedDependencies(t, tc, deps)
				})
			})
		}
	})
}
