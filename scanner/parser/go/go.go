package gosum

import (
	"bufio"
	"fmt"
	"io"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Parse scans a go.sum file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	set := make(map[string]bool)
	result := make([]parser.Package, 0)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		parts := strings.Fields(scanner.Text())
		if len(parts) != 3 {
			continue
		}
		name, version := parts[0], parts[1]
		version = strings.TrimSuffix(version, "/go.mod")
		version = strings.TrimSuffix(version, "+incompatible")
		id := fmt.Sprintf("%s:%s", name, version)
		if set[id] {
			continue
		}
		result = append(result, parser.Package{Name: name, Version: version})
		set[id] = true
	}
	return result, nil, nil
}

func init() {
	parser.Register("go", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeGo,
		Filenames:   []string{"go.sum"},
	})
}
