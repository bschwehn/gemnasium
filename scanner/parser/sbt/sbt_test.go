package sbt

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestParse(t *testing.T) {
	cases := []string{"big", "small", "self-edge"}
	for _, tc := range cases {
		fixture := testutil.Fixture(t, tc, "dependencies-compile.dot")
		pkgs, deps, err := Parse(fixture)
		require.NoError(t, err)

		t.Run("packages", func(t *testing.T) {
			testutil.RequireExpectedPackages(t, tc, pkgs)
		})

		t.Run("dependencies", func(t *testing.T) {
			testutil.RequireExpectedDependencies(t, tc, deps)
		})
	}
}

// TestRootNodeRemoval builds two graphs:
// - a raw DOT file (output of dependencyDot task), parsed with readGraph
// - a simplified DOT with no root node, parsed with newDotGraph
// We should get the same packages and dependencies
// after parsing these two graphs.
func TestRootNodeRemoval(t *testing.T) {
	cases := []struct {
		name                 string
		graphWithProjectRoot string
		expectedGraph        string
	}{
		{
			"root node removal",
			`digraph "dependency-graph" {
					"com.gitlab.root:root:1" -> "com.gitlab.a:a:x.y.z"
					"com.gitlab.root:root:1" -> "com.gitlab.b:b:a.b.c"
					"com.gitlab.a:a:x.y.z" -> "com.gitlab.a:a2:x.y.z1"
					"com.gitlab.b:b:a.b.c" -> "com.gitlab.b:b2:a.b.c-rc"
			}`,
			`digraph "dependency-graph" {
					"com.gitlab.a:a:x.y.z" -> "com.gitlab.a:a2:x.y.z1"
					"com.gitlab.b:b:a.b.c" -> "com.gitlab.b:b2:a.b.c-rc"
			}`,
		},
		{
			"no nodes",
			`digraph "dependency-graph" {
			}`,
			`digraph "dependency-graph" {
			}`,
		},
		{
			"only root node",
			`digraph "dependency-graph" {
					"com.gitlab.root:root:1"
					"com.gitlab.root:root:1"
			}`,
			`digraph "dependency-graph" {
			}`,
		},
	}

	for _, tc := range cases {
		g, err := readGraph(strings.NewReader(tc.graphWithProjectRoot))
		require.NoError(t, err)
		g2, err := newDotGraph([]byte(tc.expectedGraph))

		require.NoError(t, err)
		apkgs, adeps, err := parseGraph(g)
		require.NoError(t, err)
		bpkgs, bdeps, err := parseGraph(g2)
		require.NoError(t, err)

		require.ElementsMatchf(t, apkgs, bpkgs, tc.name)
		require.ElementsMatchf(t, adeps, bdeps, tc.name)
	}
}
