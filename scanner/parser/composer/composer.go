package composer

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Document contains a list of Packages
type Document struct {
	Packages []Package `json:"packages"`
}

// Package contains info about the composer
type Package struct {
	Name    string `json:"name"`
	Version string `json:"version"` // Installed version
}

// Parse scans a Composer lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}

	result := []parser.Package{}
	for _, p := range document.Packages {
		result = append(result, parser.Package{Name: p.Name, Version: p.Version})
	}
	return result, nil, nil
}

func init() {
	parser.Register("composer", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePackagist,
		Filenames:   []string{"composer.lock"},
	})
}
