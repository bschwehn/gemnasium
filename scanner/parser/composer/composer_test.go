package composer

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestComposer(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"simple", "big"} {
			t.Run(tc, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc, "composer.lock")
				got, _, err := Parse(fixture)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc, got)
			})
		}
	})
}
