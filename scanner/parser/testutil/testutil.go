package testutil

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Fixture supplies an io.Reader for a file stored in scanner/parser/<parserType>/fixtures/**
func Fixture(t *testing.T, d, f string) io.Reader {
	path := filepath.Join("fixtures", d, f)

	file, err := os.Open(path)
	require.NoErrorf(t, err, "fixture not found %s", path)
	defer file.Close()

	var res bytes.Buffer
	_, err = io.Copy(&res, file) // read fixture into separate buffer in order to handle any errors here
	require.NoErrorf(t, err, "cannot read fixture file %s", path)

	return &res
}

// RequireExpectedPackages tests the supplied package list against the spectation stored at
// scanner/parser/<parserType>/expect/*/packages.json
// This function generates a test failure if the lists don't match.
// If the expected json file is not found then it is created using the actual param.
func RequireExpectedPackages(t *testing.T, dir string, actual []parser.Package) {
	path := filepath.Join("expect", dir, "packages.json")

	file := openOrCreateExpectation(t, path, actual)
	defer file.Close()

	expected := []parser.Package{}
	err := json.NewDecoder(file).Decode(&expected)
	require.NoErrorf(t, err, "expectation json could not be decoded %s", path)
	require.ElementsMatch(t, expected, actual)
}

// RequireExpectedDependencies returns the parser.Dependency list stored at particular path
// scanner/parser/<parserType>/expect/*/dependencies.json
// This function generates a test failure if the lists don't match.
// If the expected json file is not found then it is created using the actual param.
func RequireExpectedDependencies(t *testing.T, dir string, actual []parser.Dependency) {
	path := filepath.Join("expect", dir, "dependencies.json")

	file := openOrCreateExpectation(t, path, actual)
	defer file.Close()

	expected := []parser.Dependency{}
	err := json.NewDecoder(file).Decode(&expected)
	require.NoErrorf(t, err, "expectation json could not be decoded at %s", path)
	require.ElementsMatch(t, expected, actual)
}

func openOrCreateExpectation(t *testing.T, path string, ifmissing interface{}) *os.File {
	file, err := os.Open(path)
	if err == nil {
		return file
	}

	perr := err.(*os.PathError)
	require.ErrorIsf(t, err, perr, "unknown error when opening expectation at %s", path)

	createExpectation(t, path, ifmissing)

	return nil
}

func createExpectation(t *testing.T, path string, content interface{}) {
	err := os.MkdirAll(filepath.Dir(path), 0755)
	require.NoErrorf(t, err, "could not create expectation dir %s", filepath.Dir(path))

	f, err := os.Create(path)
	require.NoErrorf(t, err, "could not open newly created expectation file at %s", path)
	defer f.Close()

	var out bytes.Buffer
	enc := json.NewEncoder(&out)
	enc.SetIndent("", "  ")
	err = enc.Encode(content)
	require.NoErrorf(t, err, "could not marshal json content from %s", path)

	_, err = out.WriteTo(f)
	require.NoErrorf(t, err, "could not write new expectation file to %s", path)

	t.Fatalf("expectation not found but was automatically created based on supplied fixture at %s", path)
}
