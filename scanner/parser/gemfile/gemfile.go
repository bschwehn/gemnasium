package gemfile

import (
	"bufio"
	"io"
	"regexp"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Document is a list of project dependencies
type Document struct {
	Dependencies []Dependency `json:"dependencies"`
}

// Dependency is a struct containing name version and line number
type Dependency struct {
	Name    string
	Version string // Installed version
	Line    int64
}

const (
	supportedFileFormatVersion = 1
	patternBundled             = "BUNDLED WITH"
	patternDependencies        = "DEPENDENCIES"
	patternPlatforms           = "PLATFORMS"
	patternRuby                = "RUBY VERSION"
	patternGit                 = "GIT"
	patternGem                 = "GEM"
	patternPath                = "PATH"
	patternPlugin              = "PLUGIN SOURCE"
	patternSpecs               = "  specs:"

	stateSource      = "source"
	stateDependency  = "dependency"
	statePlatform    = "platform"
	stateRuby        = "ruby"
	stateBundledWith = "bundled_with"
	stateUnknown     = "unknown"
)

var patternOptionsRegexp *regexp.Regexp
var patternOtherRegexp *regexp.Regexp
var patternNameAndVersionRegexp *regexp.Regexp

// SourceType is the name of a type
type SourceType struct {
	Name string
}

// Parse scans a Bundler lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	document.Dependencies = []Dependency{}

	depfile := bufio.NewScanner(r)

	var line string
	var lineNumber int64
	var state = stateUnknown
	var currentSourceType = SourceType{}
	for depfile.Scan() {
		line = depfile.Text()
		lineNumber++

		switch {
		case line == patternGit || line == patternGem || line == patternPath || line == patternPlugin:
			state = stateSource
			parseSource(&document, &currentSourceType, line, lineNumber)

		case line == patternDependencies:
			state = stateDependency

		case line == patternPlatforms:
			state = statePlatform

		case line == patternRuby:
			state = stateRuby

		case line == patternBundled:
			state = stateBundledWith

		case patternOtherRegexp.MatchString(line):
			state = stateUnknown

		case state != stateUnknown:
			switch state {
			case stateSource:
				parseSource(&document, &currentSourceType, line, lineNumber)
			case stateDependency:
				// TODO
			case statePlatform:
				// TODO
			case stateRuby:
				// TODO
			case stateBundledWith:
				// TODO
			}
		}
	}

	result := make([]parser.Package, len(document.Dependencies))
	for i, dep := range document.Dependencies {
		result[i] = parser.Package{Name: dep.Name, Version: dep.Version}
	}
	return result, nil, nil
}

func init() {
	// register parser
	parser.Register("gemfile", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeGem,
		Filenames:   []string{"Gemfile.lock", "gems.locked"},
	})

	// compile regexps
	patternOtherRegexp = regexp.MustCompile(`^[^\s]`)
	patternOptionsRegexp = regexp.MustCompile(`(?i)^  ([a-z]+): (.*)$`)
	patternNameAndVersionRegexp = regexp.MustCompile(`^( {2}| {4}| {6})([^\s]*?)(?: \(([^-]*)(?:-(.*))?\))?(\!)?$`)
}

func parseSource(document *Document, currentSourceType *SourceType, line string, lineNumber int64) {
	switch {
	case line == patternSpecs:
		switch currentSourceType.Name {
		case patternPath:
		case patternGit:
		case patternGem:
		case patternPlugin:
		}
	case patternOptionsRegexp.MatchString(line):

	case line == patternGit || line == patternGem || line == patternPath || line == patternPlugin:
		currentSourceType.Name = line
	default:
		if currentSourceType.Name != patternGem {
			return
		}
		parseSpec(document, line, lineNumber)
	}
}

func parseSpec(document *Document, line string, lineNumber int64) {
	var matches []string
	if matches = patternNameAndVersionRegexp.FindStringSubmatch(line); matches == nil {
		return
	}

	spaces := matches[1]
	name := matches[2]
	version := matches[3]
	// FIXME: integrate platform into dependency
	// platform := matches[4]

	switch len(spaces) {
	case 4:
		document.Dependencies = append(document.Dependencies, Dependency{Name: name, Line: lineNumber, Version: version})
	case 6:
		// TODO: parse the dependencies of current spec
	}
}
