module gitlab.com/gitlab-org/security-products/tests/go-modules

go 1.12

require (
	github.com/astaxie/beego v1.10.0
	github.com/minio/minio v0.0.0-20180419184637-5a16671f721f
	github.com/minio/minio-go v6.0.14+incompatible // indirect
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
