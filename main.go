package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/libfinder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/remediate"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/gem"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/php"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/python"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/semver"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/composer"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/conan"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/go"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

const (
	flagTargetDir        = "target-dir"
	flagArtifactDir      = "artifact-dir"
	flagRemediate        = "remediate"
	flagRemediateTimeout = "remediate-timeout"
	flagScanLibs         = "scan-libs"

	defaultTimeoutRemediate = 5 * time.Minute
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []*cli.Command{
		findCommand(),
		runCommand(),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func findCommand() *cli.Command {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    flagScanLibs,
			Usage:   "Scan vendored libraries",
			EnvVars: []string{"GEMNASIUM_LIBRARY_SCAN_ENABLED"},
			Value:   false,
		},
	}
	flags = append(flags, finder.Flags(finder.PresetGemnasium)...)
	flags = append(flags, libfinder.Flags()...)

	return &cli.Command{
		Name:      "find",
		Aliases:   []string{"f"},
		Usage:     "Find compatible files in a directory",
		ArgsUsage: "[directory]",
		Flags:     flags,
		Action: func(c *cli.Context) error {
			// one argument is expected
			if c.Args().Len() != 1 {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}
			dir := c.Args().Get(0)

			// find dependency files
			projFinder, err := finder.NewFinder(c, finder.PresetGemnasium)
			if err != nil {
				return err
			}
			projects, err := projFinder.FindProjects(dir)
			if err != nil {
				return err
			}
			for _, project := range projects {
				for _, file := range project.Files {
					fmt.Println(project.FilePath(file))
				}
			}

			// find library files
			if c.Bool(flagScanLibs) {
				libFinder, err := libfinder.NewFinder(c)
				if err != nil {
					return err
				}
				libs, err := libFinder.FindLibraries(dir)
				if err != nil {
					return err
				}
				for _, lib := range libs {
					fmt.Println(lib.Path)
				}
			} else {
				log.Warn("skip vendored libraries")
			}

			return nil
		},
	}
}

func runCommand() *cli.Command {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:    flagTargetDir,
			Usage:   "Target directory",
			EnvVars: []string{command.EnvVarTargetDir, command.EnvVarCIProjectDir},
		},
		&cli.StringFlag{
			Name:    flagArtifactDir,
			Usage:   "Artifact directory",
			EnvVars: []string{command.EnvVarArtifactDir, command.EnvVarCIProjectDir},
		},
		&cli.BoolFlag{
			Name:    flagScanLibs,
			Usage:   "Scan vendored libraries",
			EnvVars: []string{"GEMNASIUM_LIBRARY_SCAN_ENABLED"},
			Value:   false,
		},
		&cli.BoolFlag{
			Name:    flagRemediate,
			Usage:   "Remediate vulnerabilities",
			EnvVars: []string{"DS_REMEDIATE"},
			Value:   true,
		},
		&cli.DurationFlag{
			Name:    flagRemediateTimeout,
			EnvVars: []string{"DS_REMEDIATE_TIMEOUT"},
			Usage:   "Time limit for vulnerabilities auto-remediation",
			Value:   defaultTimeoutRemediate,
		},
	}

	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, finder.Flags(finder.PresetGemnasium)...)
	flags = append(flags, libfinder.Flags()...)
	flags = append(flags, scanner.Flags()...)
	flags = append(flags, vrange.Flags()...)

	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			startTime := report.ScanTime(time.Now())

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// find dependency files
			projFinder, err := finder.NewFinder(c, finder.PresetGemnasium)
			if err != nil {
				return err
			}
			projects, err := projFinder.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			// scan dependency files
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, projects)
			if err != nil {
				return err
			}

			log.Debugf("%s", "In main.go after of ScanProjects")
			// scan lib files
			if c.Bool(flagScanLibs) {
				libFinder, err := libfinder.NewFinder(c)
				log.Debugf("%s", "In main.go NewFinder")
				if err != nil {
					return err
				}
				libs, err := libFinder.FindLibraries(targetDir)
				log.Debugf("%s", "In main.go FindLibraries")
				if err != nil {
					return err
				}
				libFiles, err := scanner.ScanLibs(targetDir, libs)
				log.Debugf("%s", "In main.go ScanLibs")
				if err != nil {
					return err
				}
				result = append(result, libFiles...)
			} else {
				log.Warn("skip vendored libraries")
			}

			// convert to generic report
			var prependPath = "" // empty because the analyzer scans the root directory
			var report = convert.ToReport(result, prependPath, &startTime)

			// remediate vulnerabilities
			if c.Bool(flagRemediate) {
				if !isGitClone(targetDir) {
					log.Warn("auto-remediation requires a valid git directory") // don't fail
				} else {
					var t = c.Duration(flagRemediateTimeout)
					ctx, cancel := context.WithTimeout(context.Background(), t)
					defer cancel()
					report.Remediations = remediations(ctx, result...)
				}
			}

			report.Sort()

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}

// remediations attempts to cure affected dependency files and returns remediations.
func remediations(ctx context.Context, scanFiles ...scanner.File) []report.Remediation {
	var allRems = make([]report.Remediation, 0)
	for _, file := range scanFiles {
		// attempt to cure
		cures, err := remediate.Remediate(ctx, file)
		switch err {
		case nil:
			// proceed
		case context.DeadlineExceeded:
			// report timeout and proceed
			log.Error("timeout exceeded during auto-remediation")
		default:
			// report error and move on to the next dependency file;
			// dependency scanning must not fail when remediation fails
			log.Print(err)
			continue
		}

		// convert cures to remediations;
		// it can't be extracted out of this loop because dependency file is required.
		var rems = make([]report.Remediation, len(cures))
		for i, cure := range cures {
			var refs = make([]report.Ref, len(cure.Affections))
			for j, a := range cure.Affections {
				// HACK convert to an issue to create a reference
				var vuln = convert.VulnerabilityConverter{
					FilePath:   file.Path,
					Advisory:   a.Advisory,
					Dependency: a.Dependency,
				}.Issue()

				refs[j] = report.NewRef(vuln)
			}
			rems[i] = report.Remediation{
				Fixes:   refs,
				Summary: cure.Summary,
				Diff:    base64.StdEncoding.EncodeToString(cure.Diff),
			}
		}
		allRems = append(allRems, rems...)
	}
	return allRems
}

func isGitClone(dir string) bool {
	cmd := exec.Command("git", "-C", dir, "status")
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err == nil
}
