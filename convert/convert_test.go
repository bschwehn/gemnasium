package convert

import (
	"bytes"
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestConvert(t *testing.T) {

	// advisories
	pgAdvisory := advisory.Advisory{
		UUID:        "7d9ba955-fd99-4503-936e-f6833768f76e",
		Package:     advisory.Package{Type: "gem", Name: "pg"},
		Identifiers: []string{"CVE-1234", "GHSA-xpmx-h7xq-xffh"},
		Identifier:  "CVE-1234",
		Title:       "Regular Expression Denial of Service",
		Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
		Solution:    "Upgrade to latest version.",
		Links:       []string{"https://security.io/advisories/119", "https://security.io/advisories/117", "https://security.io/advisories/118"},
		URL:         "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
	}

	// expected scan information
	scan := report.Scan{
		Scanner: report.ScannerDetails{
			ID:   "gemnasium",
			Name: "Gemnasium",
			URL:  "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
			Vendor: report.Vendor{
				Name: "GitLab",
			},
			Version: metadata.ScannerVersion,
		},
		Type:   "dependency_scanning",
		Status: report.StatusSuccess,
	}

	// test cases
	tcs := []struct {
		name           string
		envDepPathMode string
		files          []scanner.File
		report         *report.Report
	}{
		{
			"two files, no graph",
			"",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: report.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "pg", Version: "0.8.0"},
						{Name: "puma", Version: "2.16.0"},
					},
					Affections: []scanner.Affection{
						{
							Advisory:   pgAdvisory,
							Dependency: parser.Package{Name: "pg", Version: "0.8.0"},
						},
					},
				},
				{
					Path:           "node/yarn.lock",
					PackageManager: report.PackageManagerYarn,
					PackageType:    "npm",
					Packages: []parser.Package{
						{Name: "acorn", Version: "4.0.4"},
						{Name: "acorn", Version: "3.3.0"},
						{Name: "acorn", Version: "4.0.11"},
						{Name: "@angular/animations", Version: "4.4.6"},
					},
				},
			},
			&report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:    metadata.Type,
						Scanner:     metadata.IssueScanner,
						Name:        "Regular Expression Denial of Service",
						Message:     "Regular Expression Denial of Service in pg",
						Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
						CompareKey:  "app/rails/Gemfile.lock:pg:gemnasium:7d9ba955-fd99-4503-936e-f6833768f76e",
						Severity:    report.SeverityLevelUnknown,
						Solution:    "Upgrade to latest version.",
						Location: report.Location{
							File: "app/rails/Gemfile.lock",
							Dependency: &report.Dependency{
								Package: report.Package{Name: "pg"},
								Version: "0.8.0",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gemnasium",
								Name:  "Gemnasium-7d9ba955-fd99-4503-936e-f6833768f76e",
								Value: "7d9ba955-fd99-4503-936e-f6833768f76e",
								URL:   "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
							},
							{
								Type:  "cve",
								Name:  "CVE-1234",
								Value: "CVE-1234",
								URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1234",
							},
							{
								Type:  "ghsa",
								Name:  "GHSA-xpmx-h7xq-xffh",
								Value: "GHSA-xpmx-h7xq-xffh",
								URL:   "https://github.com/advisories/GHSA-xpmx-h7xq-xffh",
							},
						},
						Links: []report.Link{
							{URL: "https://security.io/advisories/117"},
							{URL: "https://security.io/advisories/118"},
							{URL: "https://security.io/advisories/119"},
						},
					},
				},
				DependencyFiles: []report.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []report.Dependency{
							{Package: report.Package{Name: "pg"}, Version: "0.8.0"},
							{Package: report.Package{Name: "puma"}, Version: "2.16.0"},
						},
					},
					{
						Path:           "app/node/yarn.lock",
						PackageManager: "yarn",
						Dependencies: []report.Dependency{
							{Package: report.Package{Name: "acorn"}, Version: "4.0.4"},
							{Package: report.Package{Name: "acorn"}, Version: "3.3.0"},
							{Package: report.Package{Name: "acorn"}, Version: "4.0.11"},
							{Package: report.Package{Name: "@angular/animations"}, Version: "4.4.6"},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, graph",
			"",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: report.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "pg", Version: "0.8.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "pg", Version: "0.8.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
					Affections: []scanner.Affection{
						{
							Advisory:   pgAdvisory,
							Dependency: parser.Package{Name: "pg", Version: "0.8.0"},
						},
					},
				},
			},
			&report.Report{
				Version: report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{
					{
						Category:    metadata.Type,
						Scanner:     metadata.IssueScanner,
						Name:        "Regular Expression Denial of Service",
						Message:     "Regular Expression Denial of Service in pg",
						Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
						CompareKey:  "app/rails/Gemfile.lock:pg:gemnasium:7d9ba955-fd99-4503-936e-f6833768f76e",
						Severity:    report.SeverityLevelUnknown,
						Solution:    "Upgrade to latest version.",
						Location: report.Location{
							File: "app/rails/Gemfile.lock",
							Dependency: &report.Dependency{
								IID:     2,
								Package: report.Package{Name: "pg"},
								Version: "0.8.0",
							},
						},
						Identifiers: []report.Identifier{
							{
								Type:  "gemnasium",
								Name:  "Gemnasium-7d9ba955-fd99-4503-936e-f6833768f76e",
								Value: "7d9ba955-fd99-4503-936e-f6833768f76e",
								URL:   "https://gitlab.com/gemnasium-db/-/blob/master/gem/pg/CVE-1234.yml",
							},
							{
								Type:  "cve",
								Name:  "CVE-1234",
								Value: "CVE-1234",
								URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1234",
							},
							{
								Type:  "ghsa",
								Name:  "GHSA-xpmx-h7xq-xffh",
								Value: "GHSA-xpmx-h7xq-xffh",
								URL:   "https://github.com/advisories/GHSA-xpmx-h7xq-xffh",
							},
						},
						Links: []report.Link{
							{URL: "https://security.io/advisories/117"},
							{URL: "https://security.io/advisories/118"},
							{URL: "https://security.io/advisories/119"},
						},
					},
				},
				DependencyFiles: []report.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []report.Dependency{
							{
								IID:     1,
								Package: report.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								IID:     2,
								Package: report.Package{Name: "pg"},
								Version: "0.8.0",
								DependencyPath: []report.DependencyRef{
									{IID: 1},
								},
							},
							{
								IID:     3,
								Package: report.Package{Name: "activerecord"},
								Version: "5.1",
							},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, no affection, path to all",
			"all",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: report.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
				},
			},
			&report.Report{
				Version:         report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{},
				DependencyFiles: []report.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []report.Dependency{
							{
								IID:     1,
								Direct:  true,
								Package: report.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								IID:     2,
								Package: report.Package{Name: "activerecord"},
								Version: "5.1",
								DependencyPath: []report.DependencyRef{
									{IID: 1},
								},
							},
						},
					},
				},
				Scan: scan,
			},
		},

		{
			"single file, no affection, no path",
			"none",
			[]scanner.File{
				{
					Path:           "rails/Gemfile.lock",
					PackageManager: report.PackageManagerBundler,
					PackageType:    "gem",
					Packages: []parser.Package{
						{Name: "postgres-copy", Version: "1.5.0"},
						{Name: "activerecord", Version: "5.1"},
					},
					Dependencies: []parser.Dependency{
						{
							To: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
						},
						{
							From: &parser.Package{Name: "postgres-copy", Version: "1.5.0"},
							To:   &parser.Package{Name: "activerecord", Version: "5.1"},
						},
					},
				},
			},
			&report.Report{
				Version:         report.CurrentVersion(),
				Vulnerabilities: []report.Vulnerability{},
				DependencyFiles: []report.DependencyFile{
					{
						Path:           "app/rails/Gemfile.lock",
						PackageManager: "bundler",
						Dependencies: []report.Dependency{
							{
								Package: report.Package{Name: "postgres-copy"},
								Version: "1.5.0",
							},
							{
								Package: report.Package{Name: "activerecord"},
								Version: "5.1",
							},
						},
					},
				},
				Scan: scan,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			// prepare input reader
			var input bytes.Buffer
			enc := json.NewEncoder(&input)
			enc.SetIndent("", "  ")
			err := enc.Encode(tc.files)
			require.NoError(t, err)

			r := bytes.NewReader(input.Bytes())

			// convert
			os.Setenv("DS_DEPENDENCY_PATH_MODE", tc.envDepPathMode)
			prependPath := "app"
			got, err := Convert(r, prependPath)
			require.NoError(t, err)

			// compare
			want := tc.report
			require.Equal(t, want, got)
		})
	}
}
