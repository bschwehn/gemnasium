package convert

import (
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

// DepPathMode is the strategy used to enable dependency paths
type DepPathMode int

const (
	// DepPathModeNone is when dependency paths are disabled
	DepPathModeNone = DepPathMode(iota)

	// DepPathModeAffected is when dependency paths are enabled
	// only for packages that are affected
	DepPathModeAffected

	// DepPathModeAll is when dependency paths are enabled
	// for all dependencies
	DepPathModeAll
)

// Config configures how a scanned file is converted.
type Config struct {
	DepPathMode DepPathMode // DepPathMode is the strategy used to enable dependency paths
	PrependPath string      // PrependPath is prepended to the file path
}

// setDepPathFunc is the type of a function that tells if the dependency path
// should be rendered for a given package
type setDepPathFunc func(parser.Package) bool

// NewFileConverter returns a new FileConverter
// where the package index and the dependency graph have been initialized if needed.
func NewFileConverter(f scanner.File, cfg Config) *FileConverter {
	var index *Index              // index used to turn packages into IIDs
	var graph *Graph              // graph used to calculate dependency paths
	var setDepIID bool            // true if all dependencies should have IIDs
	var setDepPath setDepPathFunc // true if given package should have a dependency path

	// configure setDepIID
	switch cfg.DepPathMode {
	case DepPathModeNone:
		setDepIID = false

	case DepPathModeAffected, DepPathModeAll:
		// set IIDs for all dependencies; a dependency might be referrenced
		// in the path to a vulnerable dependency if it's not vulnerable
		setDepIID = true

		// build index and graph
		index = NewIndex(f.Packages)
		graph = NewGraph(f.Packages, f.Dependencies, index)
	}

	// configure setDepPath
	switch cfg.DepPathMode {
	case DepPathModeNone:
		// never set dependency path
		setDepPath = func(parser.Package) bool {
			return false
		}

	case DepPathModeAffected:
		// build map of affected packages
		affectedMap := map[parser.Package]bool{}
		for _, aff := range f.Affections {
			affectedMap[aff.Dependency] = true
		}
		// set dependency path if package is affected
		setDepPath = func(pkg parser.Package) bool {
			_, affected := affectedMap[pkg]
			return affected
		}

	case DepPathModeAll:
		// always set dependency path
		setDepPath = func(parser.Package) bool {
			return true
		}
	}

	return &FileConverter{
		prependPath: cfg.PrependPath,
		setDepIID:   setDepIID,
		setDepPath:  setDepPath,
		File:        f,
		index:       index,
		graph:       graph,
	}
}

// FileConverter is used to render a file processed by the scanner
// into a list of vulnerability objects and a dependency file object,
// to be included in the JSON report.
type FileConverter struct {
	prependPath string
	setDepIID   bool
	setDepPath  setDepPathFunc
	File        scanner.File
	index       *Index
	graph       *Graph
}

// DependencyFile generates a dependency file object
// to be included in the JSON report.
func (c FileConverter) DependencyFile() report.DependencyFile {
	return report.DependencyFile{
		Path:           filepath.Join(c.prependPath, c.File.Path),
		Dependencies:   c.dependencies(),
		PackageManager: c.packageManager(),
	}
}

// packageManager converts the type and value of the package manager.
// "gradle" is converted to "maven" when environment variable
// "DS_REPORT_PACKAGE_MANAGER_MAVEN_WHEN_JAVA" is true.
// "pipenv" and "setuptools" are converted to "pip" when environment variable
// "DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON" is true.
func (c FileConverter) packageManager() report.PackageManager {
	pkgr := report.PackageManager(c.File.PackageManager)
	switch pkgr {
	case "gradle":
		// parse bool flag like in https://github.com/urfave/cli/blob/v2.3.0/flag_bool.go#L65
		val := os.Getenv("DS_REPORT_PACKAGE_MANAGER_MAVEN_WHEN_JAVA")
		if valBool, _ := strconv.ParseBool(val); valBool {
			return "maven"
		}
	case "pipenv", "setuptools":
		// parse bool flag like in https://github.com/urfave/cli/blob/v2.3.0/flag_bool.go#L65
		val := os.Getenv("DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON")
		if valBool, _ := strconv.ParseBool(val); valBool {
			return "pip"
		}
	}
	return pkgr
}

// dependencies generates a slice of dependency objects
// to be included in the JSON report.
func (c FileConverter) dependencies() []report.Dependency {
	deps := []report.Dependency{}
	for _, pkg := range c.File.Packages {
		dep := report.Dependency{
			Package: report.Package{Name: pkg.Name},
			Version: pkg.Version,
		}

		if c.setDepIID {
			dep.IID = c.index.IDOf(pkg)
		}

		if c.setDepPath(pkg) {
			path := c.graph.PathTo(pkg)
			if len(path) == 0 {
				dep.Direct = true
			}
			for _, node := range path {
				ref := report.DependencyRef{IID: node.IID}
				dep.DependencyPath = append(dep.DependencyPath, ref)
			}
		}

		deps = append(deps, dep)
	}
	return deps
}

// Vulnerabilities generates a slice of vulnerability objects
// to be included in the JSON report.
func (c FileConverter) Vulnerabilities() []report.Vulnerability {
	filePath := filepath.Join(c.prependPath, c.File.Path)
	vulns := make([]report.Vulnerability, len(c.File.Affections))
	for i, affection := range c.File.Affections {

		var iid uint
		pkg := affection.Dependency
		if c.setDepIID {
			iid = c.index.IDOf(pkg)
		}

		c := VulnerabilityConverter{
			FilePath:      filePath,
			Advisory:      affection.Advisory,
			Dependency:    pkg,
			DependencyIID: iid,
		}
		vulns[i] = c.Issue()
	}
	return vulns
}
