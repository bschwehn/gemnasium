package convert

import (
	"encoding/json"
	"io"
	"os"
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	identifierTypeGemnasiumUUID  = report.IdentifierType("gemnasium")
	identifierPrefixGemnasiuUUID = "Gemnasium-"

	gemnasiumURL = "https://deps.sec.gitlab.com"

	// envDepPathMode is the name of the environment variable
	// that controls how dependency paths are rendered (EXPERIMENTAL)
	envDepPathMode = "DS_DEPENDENCY_PATH_MODE"
)

// Convert converts the output of the Gemnasium (list of dependency files) to a report.
func Convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var result []scanner.File
	err := json.NewDecoder(reader).Decode(&result)
	if err != nil {
		return nil, err
	}
	return ToReport(result, prependPath, nil), nil
}

// ToReport converts dependency files returned by the Gemnasium scanner to a report.
func ToReport(scanFiles []scanner.File, prependPath string, startTime *report.ScanTime) *report.Report {
	// collect vulnerabilities and dependency files
	vulns := []report.Vulnerability{}
	depfiles := []report.DependencyFile{}
	for _, scanFile := range scanFiles {
		cfg := Config{
			PrependPath: prependPath,
			DepPathMode: depPathModeForFile(scanFile),
		}

		// add affections as vulnerabilities
		c := NewFileConverter(scanFile, cfg)
		vulns = append(vulns, c.Vulnerabilities()...)

		// add as dependency file if handled by package manager
		if scanFile.PackageManager != "" {
			depfiles = append(depfiles, c.DependencyFile())
		}
	}

	vulnerabilityReport := report.NewReport()
	vulnerabilityReport.Vulnerabilities = vulns
	vulnerabilityReport.DependencyFiles = depfiles
	vulnerabilityReport.Scan.Scanner = metadata.ReportScanner
	vulnerabilityReport.Scan.Type = metadata.Type
	vulnerabilityReport.Scan.Status = report.StatusSuccess
	if startTime != nil {
		vulnerabilityReport.Scan.StartTime = startTime
		endTime := report.ScanTime(time.Now())
		vulnerabilityReport.Scan.EndTime = &endTime
	}

	return &vulnerabilityReport
}

func depPathModeForFile(f scanner.File) DepPathMode {
	if len(f.Dependencies) == 0 {
		// dependency graph information not available
		return DepPathModeNone
	}

	switch os.Getenv(envDepPathMode) {
	case "all":
		return DepPathModeAll
	case "none":
		return DepPathModeNone
	default:
		return DepPathModeAffected
	}
}
